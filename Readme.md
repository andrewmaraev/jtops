jtops
=====

Small utility to generate list of worked issues per day from JIRA

Usage
-----

jtops <jira login> <jira password> <jira user> > report.txt

<jira user> is the user for whom you want to generate report
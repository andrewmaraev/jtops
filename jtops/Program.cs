﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace jtops
{
	class Program
	{
		private const string JiraURL = "http://jira.development.dictate.it:8080";
		private const string SearchURL = JiraURL + "/rest/api/2/search";
		private const string IssueURL = JiraURL + "/rest/api/2/issue";

		static WebHeaderCollection GetHeaders(string login, string password)
		{
			var key = Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Format("{0}:{1}", login, password)));
			var result = new WebHeaderCollection { { "Authorization", String.Format("Basic {0}", key) } };
			return result;
		}


		static void Main(string[] args)
		{
			var login = args[0];
			var password = args[1];
			var user = args.Length >= 3 ? args[2] : login;

			var today = DateTime.Now.Date;
			var lastWeekStart = today.AddDays(-7).AddDays(-(int) today.DayOfWeek);
			var lastWeekEnd = lastWeekStart.AddDays(7);

			var currentDate = lastWeekStart;
			while (currentDate < lastWeekEnd)
			{
				var tasks =
					(IEnumerable<dynamic>)Search(String.Format("key in workedIssues(\"{0:yyyy-MM-dd}\", \"{0:yyyy-MM-dd}\", \"{1}\")", currentDate, user), login, password).Result.issues;
				var keys = tasks.Select(x => (string) x.key + " - " + (string)x.fields.summary).OrderBy(x => x).ToArray();
				if (keys.Any())
				{
					Console.WriteLine("{0:yyyy-MM-dd}\n{1}\n", currentDate, String.Join("\n", keys));
				}
				currentDate = currentDate.AddDays(1);
			}
		}

		async static Task<dynamic> Search(string jql, string login, string password)
		{
			int startAt = 0;
			const int maxResults = 500;

			var result = await Search(jql, startAt, maxResults, login, password);

			while ((int)result.issues.Count < (int)result.total)
			{
				startAt += maxResults;
				var partResult = await Search(jql, startAt, maxResults, login, password);
				var allIssues = new List<dynamic>(result.issues);
				allIssues.AddRange(partResult.issues);
				result.issues = new JArray(allIssues.ToArray());
			}

			result.issues = new JArray(((IEnumerable<dynamic>)result.issues).OrderBy(x => x.key));

			return result;
		}

		async static Task<dynamic> Search(string jql, int startAt, int maxResults, string login, string password)
		{
			var request = new
			{
				jql,
				fields = new[]
				{
					"summary",
					"created",
					"status",
					"priority",
					"resolutiondate",
					"worklog",
					"timespent",
					"timeestimate",
					"timeoriginalestimate",
					"aggregatetimespent",
					"aggregatetimeoriginalestimate",
					"aggregatetimeestimate",
					"issuetype"
				},
				maxResults,
				startAt,
				expand = new[] { "changelog" }
			};

			var requestStr = JsonConvert.SerializeObject(request);

			var result = await Query(SearchURL, requestStr, login, password);

			foreach (var i in result.issues)
			{
				i.fields.timespent = i.fields.timespent == null ? 0L : i.fields.timespent;
				i.fields.timeestimate = i.fields.timeestimate == null ? 0L : i.fields.timeestimate;
				i.fields.timeoriginalestimate = i.fields.timeoriginalestimate == null ? 0L : i.fields.timeoriginalestimate;
				i.fields.aggregatetimespent = i.fields.aggregatetimespent == null ? 0L : i.fields.aggregatetimespent;
				i.fields.aggregatetimeoriginalestimate = i.fields.aggregatetimeoriginalestimate == null ? 0L : i.fields.aggregatetimeoriginalestimate;
				i.fields.aggregatetimeestimate = i.fields.aggregatetimeestimate == null ? 0L : i.fields.aggregatetimeestimate;
			}

			return result;

		}

		async static Task<dynamic> Query(string url, string body, string login, string password)
		{
			var webrequest = WebRequest.Create(url);
			webrequest.Headers = GetHeaders(login, password);
			webrequest.Method = "POST";
			webrequest.ContentType = "application/json";
			var requestStream = await webrequest.GetRequestStreamAsync();
			var writer = new StreamWriter(requestStream);
			writer.Write(body);
			await writer.FlushAsync();
			var response = await webrequest.GetResponseAsync();
			var reader = new StreamReader(response.GetResponseStream());
			var responseStr = await reader.ReadToEndAsync();
			return JsonConvert.DeserializeObject(responseStr);
		}


	}
}
